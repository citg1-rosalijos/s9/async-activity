package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
@RestController
@RequestMapping("/greeting")
public class AsyncApplication {

	private ArrayList<Student> students = new ArrayList<>();

	public static void main(String[] args) {
		SpringApplication.run(AsyncApplication.class, args);
	}

	@GetMapping("/welcome")
	public String welcome(@RequestParam(value="user") String user, @RequestParam(value="role") String role) {
		String message;
		switch(role) {
			case "admin":
				message = String.format("Welcome back to the class portal, Admin %s!", user);
				break;
			case "teacher":
				message = String.format("Thank you for logging in, Teacher %s!", user);
				break;
			case "student":
				message = String.format("Welcome to the class portal, %s!", user);
				break;
			default:
				message = "Role out of range!";
		}
		return message;
	}

	@GetMapping("/register")
	public String register(@RequestParam(value="id") String id, @RequestParam(value="name") String name, @RequestParam(value="course") String course) {
		students.add(new Student(id, name, course));
		return String.format("%s your id number is registered on the system!", id);
	}

	@GetMapping("/account/{id}")
	public String account(@PathVariable("id") String id) {
		String message = String.format("Your provided %s is not found in the system!", id);
		for(Student student: students) {
			if(student.getId().equals(id)){
				message = String.format("Welcome back %s! You are currently enrolled in %s.", student.getName(), student.getCourse());
				break;
			}
		}
		return message;
	}

}
